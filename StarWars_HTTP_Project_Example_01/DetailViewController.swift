//
//  DetailViewController.swift
//  StarWars_HTTP_Project_Example_01
//
//  Created by Micky on 13.11.2020.
//

import UIKit
import Alamofire

class DetailViewController: UIViewController {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var item1TitleLabel: UILabel!
    @IBOutlet weak var item1Label: UILabel!
    @IBOutlet weak var item2TitleLabel: UILabel!
    @IBOutlet weak var item2Label: UILabel!
    @IBOutlet weak var item3TitleLabel: UILabel!
    @IBOutlet weak var item3Label: UILabel!
    @IBOutlet weak var listTitleLabel: UILabel!
    
    var data: Displayable?
    var listData: [Displayable] = []
    @IBOutlet weak var listTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        commonInit()
        
        listTableView.dataSource = self
        
        fetchList()
        
        // Do any additional setup after loading the view.
    }
    
    private func commonInit() {
        guard let data = data else { return }
        
        titleLabel.text = data.titleLabelText
        subtitleLabel.text = data.subtitleLabelText
        
        item1TitleLabel.text = data.item1.label
        item1Label.text = data.item1.value
        
        item2TitleLabel.text = data.item2.label
        item2Label.text = data.item2.value
        
        item3TitleLabel.text = data.item3.label
        item3Label.text = data.item3.value
        
        listTitleLabel.text = data.listTitle
    }
}

// MARK: - UITableViewDataSource
extension DetailViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "listCell", for: indexPath)
        cell.textLabel?.text = listData[indexPath.row].titleLabelText

        return cell
    }
    
}

extension DetailViewController {
    func fetchList() {
        // 1
        guard let data = data else { return }
        
        // 2
        switch data {
        case is Film:
            fetch(data.listItems, of: Starship.self)
        case is Starship:
            fetch(data.listItems, of: Film.self)
        default:
            print("Unknown type: ", String(describing: type(of: data)))
        }
    }
    // 1
    private func fetch<T: Decodable & Displayable>(_ list: [String], of: T.Type) {
        var items: [T] = []
        // 2
        let fetchGroup = DispatchGroup()
        
        // 3
        list.forEach { (url) in
            // 4
            fetchGroup.enter()
            // 5
            AF.request(url).validate().responseDecodable(of: T.self) { (response) in
                if let value = response.value {
                    items.append(value)
                }
                // 6
                fetchGroup.leave()
            }
        }
        
        fetchGroup.notify(queue: .main) {
            self.listData = items
            self.listTableView.reloadData()
        }
    }
}

/*
 // MARK: - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
 // Get the new view controller using segue.destination.
 // Pass the selected object to the new view controller.
 }
 */


