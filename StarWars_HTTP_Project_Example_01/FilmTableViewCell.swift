//
//  FilmTableViewCell.swift
//  StarWars_HTTP_Project_Example_01
//
//  Created by Micky on 13.11.2020.
//

import UIKit

class FilmTableViewCell: UITableViewCell {
    
    @IBOutlet weak var filmTextLabel: UILabel!
    @IBOutlet weak var filmDetailTextLabel: UILabel!
    
    //    cell.textLabel?.text = item.titleLabelText
//    cell.detailTextLabel?.text = item.subtitleLabelText
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
