//
//  Starships.swift
//  StarWars_HTTP_Project_Example_01
//
//  Created by Micky on 13.11.2020.
//

import Foundation

struct Starships: Decodable {
    var count: Int
    var all: [Starship]
    
    enum CodingKeys: String, CodingKey {
        case count
        case all = "results"
    }
}
