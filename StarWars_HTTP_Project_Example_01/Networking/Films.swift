//
//  Films.swift
//  StarWars_HTTP_Project_Example_01
//
//  Created by Micky on 13.11.2020.
//

import Foundation

struct Films: Decodable {
    let count: Int
    let all: [Film]
    
    enum CodingKeys: String, CodingKey {
        case count
        case all = "results"
    }
}
