//
//  ViewController.swift
//  StarWars_HTTP_Project_Example_01
//
//  Created by Micky on 13.11.2020.
//

import UIKit
import Alamofire

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var searchBar: UISearchBar!
    var films: [Film] = []
    
    @IBOutlet weak var tableView: UITableView!
    var items: [Displayable] = []
    var selectedItem: Displayable?

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        tableView.dataSource = self
        tableView.delegate = self
        
        searchBar.delegate = self
        searchBar.showsCancelButton = true
        
        fetchFilms()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView,
                            willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        selectedItem = items[indexPath.row]
        return indexPath
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
      guard let destinationVC = segue.destination as? DetailViewController else {
        return
      }
      destinationVC.data = selectedItem
    }
  
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "FilmTableViewCell"
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? FilmTableViewCell  else {
            fatalError("The dequeued cell is not an instance of MealTableViewCell.")
        }
        
        let item = items[indexPath.row]
        cell.filmTextLabel?.text = item.titleLabelText
        cell.filmDetailTextLabel?.text = item.subtitleLabelText
        
        return cell
    }
}

extension ViewController {
    func fetchFilms() {
        // 1
        let request = AF.request("https://swapi.dev/api/films")
        // 2
        
        // MARK: With Chaining
        request
            .validate() // Ensure response returns HTTP status code in range 200-299
            .responseDecodable(of: Films.self) { response in
                guard let films = response.value else { return }
                // print(films.all[0].title) // Print first film title
                self.films = films.all
                self.tableView.reloadData()
            }
        
        // MARK: Without Chaining
//        request
//            .responseDecodable(of: Films.self) { (response) in
//
//            guard let films = response.value else { return }
//
//
//            for film in films.all {
//                print(film.title)
//            }
//            //print(films.all)
//
//        }
//
        // !!! BACKUP 13.11.2020 !!! Debugging
//        request.responseJSON { (data) in
//            print(data) }
    }
    
    func searchStarships(for name: String) {
      // 1
      let url = "https://swapi.dev/api/starships"
      // 2
      let parameters: [String: String] = ["search": name]
      // 3
      AF.request(url, parameters: parameters)
        .validate()
        .responseDecodable(of: Starships.self) { response in
          // 4
          guard let starships = response.value else { return }
          self.items = starships.all
          self.tableView.reloadData()
      }
    }

}


// MARK: - UISearchBarDelegate
extension ViewController: UISearchBarDelegate {
  func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
    
    guard let shipName = searchBar.text else { return }
    searchStarships(for: shipName)
  }
  
  func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
    searchBar.text = nil
    searchBar.resignFirstResponder()
    items = films
    tableView.reloadData()
  }
}
